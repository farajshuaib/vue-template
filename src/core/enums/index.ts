
export enum EUserRoles {
    ADMIN = 'admin',
    SUPER_ADMIN = 'super_admin',
    EDITOR = 'editor',
}

export enum EAppLocales {
    EN = 'en',
    AR = 'ar',
}

